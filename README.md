# CI

Ce dépôt contient des scripts et configuration utiles pour les déploiements sur le serveur Puppet.

L'utilisation de ces scripts est soumise à licence telle que décrit dans [LICENSE](LICENSE).
